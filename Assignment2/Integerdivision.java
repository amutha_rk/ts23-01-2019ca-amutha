package Assignment2;

public class Integerdivision {
	public static long divideAndRoundUp(long num, long divisor) {
	    if (num == 0 || divisor == 0) {
	    	return 0; 
	    	}

	    int sign = (num > 0 ? 1 : -1) * (divisor > 0 ? 1 : -1);

	    if (sign > 0) {
	        return (num + divisor - 1) / divisor;
	    }
	    else {
	        return (num / divisor);
	    }
	}

}
