package Multithreading;

public class PriorityDemo extends Thread{
	public void run() {
		System.out.println("running thread name is:"+Thread.currentThread().getName());
		System.out.println("running thread priority is:"+Thread.currentThread().getPriority());
	}

	public static void main(String[] args) {
		PriorityDemo P1=new PriorityDemo();
		PriorityDemo P2=new PriorityDemo();
		PriorityDemo P3=new PriorityDemo();
		P1.setPriority(Thread.MAX_PRIORITY);
		P1.start();
		P2.start();

	}

}
